import random

class Individual:
    modules = []
    num_modules = 0
    fitness = 0
    def __init__(self, genome_dimensions):
        self.modules = [[random.randint(0,1) for i in range(0, length_N)] for length_N in genome_dimensions]
        self.num_modules = len(genome_dimensions)

class Deme:
    population = []
    def __init__(self, pop_size, genome_dimensions,fitness_landscape):
        self.population = [Individual(genome_dimensions) for i in range(0,pop_size)]
        for indiv in self.population:
            indiv.fitness = fitness_landscape.evaluate_individual(indiv)

    def next_generation(deme):
        # crossover & mutation
        return deme

    def select_individual(deme):
        return random.choice(deme.population)
