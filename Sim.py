import Individual
import FitnessLandscape
import GeneticAlgorithm

# Init all individuals as 2N length binary random str
# Mutation 1/2 N per site
# 20 Demes of 20 Individuals
# Migration: 1 individual in each generation in each deme is randomly from another deme
# 1-point crossover
# Results are avg of 30 runs (Include 1std dv) - run until first occurance of fittest genotype
# fitness proportionate selection
# top-1 elitism
def run_simulation(genome_dimensions, pop_size, demes):
    demes = [Individual.Deme(pop_size, genome_dimensions) for i in range(0,demes)]




def print_individual():
    indv = Individual.Individual([5,5])
    print(indv.modules)
def print_landscape():
    scape = FitnessLandscape.FitnessLandscape([5,5])
    print(scape.landscape)
def evaluate_module():
    genome_dimensions = [5,5]
    indv = Individual.Individual(genome_dimensions)
    scape = FitnessLandscape.FitnessLandscape(genome_dimensions)
    print(scape.evaluate_module(indv.modules[0]))
def evaluate_genome():
    genome_dimensions = [5,5]
    indv = Individual.Individual(genome_dimensions)
    scape = FitnessLandscape.FitnessLandscape(genome_dimensions)
    print(scape.evaluate_individual(indv))
def select_individual():
    scape = FitnessLandscape.FitnessLandscape([5,5])
    deme = Individual.Deme(4, [5,5], scape)
    indiv = deme.select_individual()
    for i in range(0, 4):
        print(str(i)+":"+str(deme.select_individual().modules))
def crossover():
    scape = FitnessLandscape.FitnessLandscape([5,5])
    deme = Individual.Deme(4, [5,5], scape)
    indiv1= deme.select_individual()
    print(indiv1.modules)
    print(indiv1.modules[0])
    indiv2 = deme.select_individual()
    print(indiv2.modules)
    GeneticAlgorithm.crossover(indiv1, indiv2, [5,5])
def mutate():
    indv = Individual.Individual([2,3])
    print(indv.modules)
    GeneticAlgorithm.mutation(indv,[2,3])
    print(indv.modules)
def reproduce():
    scape = FitnessLandscape.FitnessLandscape([5,5])
    deme = Individual.Deme(4, [5,5], scape)
    indiv1= deme.select_individual()
    print(indiv1.fitness)
    indiv2 = deme.select_individual()
    print(indiv2.fitness)
    new_indv = GeneticAlgorithm.reproduce(indiv1, indiv2, [5,5], scape)
    print(new_indv.modules)
    print(new_indv.fitness)


reproduce()