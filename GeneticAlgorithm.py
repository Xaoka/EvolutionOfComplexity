import random
import Individual
import numpy


def crossover(individual1, individual2, genome_dimensions):
    new_individual = Individual.Individual(genome_dimensions) # generates unneeded DNA sequence here
    DNA_len = sum(genome_dimensions)
    crossover_point = random.randint(1,DNA_len-2)
    print("DNA_len: {0}\t\tcrossover_point: {1}".format(DNA_len,crossover_point))
    acc = 0
    for genome_index in range(0,len(genome_dimensions)):
        genome_size = genome_dimensions[genome_index]

        if(acc > crossover_point):
            new_individual.modules[genome_index] = individual2.modules[genome_index]
        if(acc + genome_size > crossover_point):
            new_individual.modules[genome_index][0:crossover_point - acc] = individual1.modules[genome_index][0:crossover_point - acc]
            new_individual.modules[genome_index][crossover_point - acc:genome_size] = individual2.modules[genome_index][crossover_point - acc:genome_size]
        else:
            new_individual.modules[genome_index] = individual1.modules[genome_index]

        acc += genome_size
    print("genome 1: "+str(individual1.modules))
    print("genome 2: "+str(individual2.modules))
    print("new genome: "+str(new_individual.modules))
    return new_individual

def mutation(individual,genome_dimensions):
    DNA_len = sum(genome_dimensions)
    for module in individual.modules:
        for locus_index in range(0,len(module)):
            if(random.randint(0,DNA_len)==0):
                module[locus_index] = 1-module[locus_index]

def reproduce(individual1, individual2, genome_dimensions, fitness_landscape):
    new_individual = crossover(individual1,individual2,genome_dimensions)
    mutation(new_individual,genome_dimensions)
    new_individual.fitness = fitness_landscape.evaluate_individual(new_individual)
    return new_individual