import numpy as numpy
import random

class FitnessLandscape:


    def __init__(self,dimensions):
        # generate randomscape
        self.landscape = [[random.random() for index in range(0,size)] for size in dimensions]


    def evaluate_individual(self,individual):
        sum = 0
        for module in individual.modules:
            sum += self.evaluate_module(module)
        return sum

    def evaluate_module(self, genome):
        unitation = 0
        for locus in genome:
            unitation += locus
        utility = (pow(2,unitation))
        print("genome utility: "+str(utility))
        return utility